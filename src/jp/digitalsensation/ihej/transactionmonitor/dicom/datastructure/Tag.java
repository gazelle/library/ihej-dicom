package jp.digitalsensation.ihej.transactionmonitor.dicom.datastructure;


public class Tag implements Comparable<Tag>
{
	private final int groupNumber;
	public int getGroupNumber()
	{
		return this.groupNumber;
	}
	
	private final int elementNumber;
	public int getElementNumber()
	{
		return this.elementNumber;
	}
	
	public Tag(int groupNumber, int elementNumber)
		throws IllegalArgumentException
	{
		if (!(0 <= groupNumber && groupNumber < 0x0000FFFF))
		{
			throw new IllegalArgumentException(
					"provided group number is not in 0 to 0xFFFF range");
		}
		
		if (!(0 <= elementNumber && elementNumber < 0x0000FFFF))
		{
			throw new IllegalArgumentException(
					"provided element number is not in 0 to 0xFFFF range");
		}
		
		this.groupNumber = groupNumber;
		this.elementNumber = elementNumber;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if (this == other)
		{
			return true;
		}
		
		if (!(other instanceof Tag))
		{
			return false;
		}
		
		Tag otherTag = (Tag)other;
		return this.groupNumber == otherTag.groupNumber && this.elementNumber == otherTag.elementNumber;
	}
	
	@Override
	public int hashCode()
	{
		return this.groupNumber * 31 + this.elementNumber;
	}

	public int compareTo(Tag other) {
		if (this.groupNumber != other.groupNumber)
		{
			return this.groupNumber - other.groupNumber;
		}
		else
		{
			return this.elementNumber - other.elementNumber;
		}
	}
	
	@Override
	public String toString()
	{
		return String.format("(%04X,%04X)", this.groupNumber, this.elementNumber);
	}
}
