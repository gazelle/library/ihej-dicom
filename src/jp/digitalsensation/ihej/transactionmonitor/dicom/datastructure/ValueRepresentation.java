package jp.digitalsensation.ihej.transactionmonitor.dicom.datastructure;

public enum ValueRepresentation
{
    /*
     *  Application Entity
     */
    AE,

    /*
     *  Age String
     */
    AS,

    /*
     *  Attribute Tag
     */
    AT,

    /*
     *  Code String
     */
    CS,

    /*
     *  Date
     */
    DA,

    /*
     *  Decimal String
     */
    DS,

    /*
     *  Date Time
     */
    DT,

    /*
     *  Floating Point Single
     */
    FL,

    /*
     *  Floating Point Double
     */
    FD,

    /*
     *  Integer String
     */
    IS,

    /*
     *  Long String
     */
    LO,

    /*
     *  Long Text
     */
    LT,

    /*
     *  Other Byte String
     */
    OB,

    /*
     *  Other Float String
     */
    OF,

    /*
     *  Other Word String
     */
    OW,

    /*
     *  Person Name
     */
    PN,

    /*
     *  Short String
     */
    SH,

    /*
     *  Signed Long
     */
    SL,

    /*
     *  Sequence of Items
     */
    SQ,

    /*
     *  Signed Short
     */
    SS,

    /*
     *  Short Text
     */
    ST,

    /*
     *  Time
     */
    TM,

    /*
     *  Unique Identifier
     */
    UI,

    /*
     *  Unsigned Long
     */
    UL,

    /*
     *  Unknown
     */
    UN,

    /*
     *  Unsigned Short
     */
    US,

    /*
     *  Unlimited Text
     */
    UT,
}
