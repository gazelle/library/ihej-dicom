package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class ImplementationVersionNameItem extends Item
{
	private String implementationVersionName;
	public String getImplementationVersionName()
	{
		return this.implementationVersionName;
	}
	
	public ImplementationVersionNameItem(int itemType, int itemLength, String implementationVersionName)
	{
		super(itemType, itemLength);
		this.implementationVersionName = implementationVersionName;
	}
}
