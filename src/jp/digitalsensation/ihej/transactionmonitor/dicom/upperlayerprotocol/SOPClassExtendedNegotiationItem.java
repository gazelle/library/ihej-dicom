package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class SOPClassExtendedNegotiationItem extends Item
{
	private int sopClassUIDLength;
	public int getSOPClassUIDLength()
	{
		return this.sopClassUIDLength;
	}
	
	private String sopClassUID;
	public String getSOPClassUID()
	{
		return this.sopClassUID;
	}
	
	private byte[] serviceClassApplicationInformation;
	public byte[] getServiceClassApplicationInformation()
	{
		return this.serviceClassApplicationInformation;
	}

	public SOPClassExtendedNegotiationItem(int itemType, int itemLength, int sopClassUIDLength, String sopClassUID, byte[] serviceClassApplicationInformation)
	{
		super(itemType, itemLength);
		this.sopClassUIDLength = sopClassUIDLength;
		this.sopClassUID = sopClassUID;
		this.serviceClassApplicationInformation = serviceClassApplicationInformation.clone();
	}

}
