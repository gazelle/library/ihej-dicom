package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class AsynchronousOperationsWindowItem extends Item
{
	private int maximumNumberOperationsInvoked;
	public int getMaximumNumberOperationsInvoked()
	{
		return this.maximumNumberOperationsInvoked;
	}
	
	private int maximumNumberOperationsPerformed;
	public int getMaximumNumberOperationsPerformed()
	{
		return this.maximumNumberOperationsPerformed;
	}
	
	public AsynchronousOperationsWindowItem(int itemType, int itemLength, int maximumNumberOperationsInvoked, int maximumNumberOperationsPerformed)
	{
		super(itemType, itemLength);
		this.maximumNumberOperationsInvoked = maximumNumberOperationsInvoked;
		this.maximumNumberOperationsPerformed = maximumNumberOperationsPerformed;
	}
}
