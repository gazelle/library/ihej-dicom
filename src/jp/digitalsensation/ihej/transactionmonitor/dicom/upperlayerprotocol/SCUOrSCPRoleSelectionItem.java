package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class SCUOrSCPRoleSelectionItem extends Item
{
	public class Role
	{
		// at A-ASSOCIATE-RQ
		public static final int NonSupport = 0x00;
		public static final int Support = 0x01;
		
		// at A-ASSOCIATE-AC
		public static final int RejectProposal = 0x00;
		public static final int AcceptProposal = 0x01;
	}
	
	private int sopClassUIDLength;
	public int getSOPClassUIDLength()
	{
		return this.sopClassUIDLength;
	}
	
	private String sopClassUID;
	public String getSOPClassUID()
	{
		return this.sopClassUID;
	}
	
	private int scuRole;
	public int getSCURole()
	{
		return this.scuRole;
	}
	
	private int scpRole;
	public int getSCPRole()
	{
		return this.scpRole;
	}
	
	public SCUOrSCPRoleSelectionItem(int itemType, int itemLength, int sopClassUIDLength, String sopClassUID, int scuRole, int scpRole)
	{
		super(itemType, itemLength);
		this.sopClassUIDLength = sopClassUIDLength;
		this.sopClassUID = sopClassUID;
		this.scuRole = scuRole;
		this.scpRole = scpRole;
	}

}
