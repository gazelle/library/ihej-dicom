package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import java.util.List;
import java.util.Collections;

public class UserInformationItem extends Item
{
	private List<Item> userData;
	public List<Item> getUserData()
	{
		return Collections.unmodifiableList(this.userData);
	}
	
	public UserInformationItem(int itemType, int itemLength, List<Item> userData)
	{
		super(itemType, itemLength);
		this.userData = userData;
	}

}
