package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class A_ABORT_PDU extends ProtocolDataUnit
{
	public static class Source
	{
		public static final int DICOMULServiceUesr = 0;
		public static final int DICOMULServiceProvider = 2;
	}
	private int source;
	public int getSource()
	{
		return this.source;
	}
	
	public static class Reason
	{
		// when source == "DICOM UL service-user"
		public static final int ReasonNotSpecified = 0;
		public static final int UnrecognizedPDU = 1;
		public static final int UnextepctedPDU = 2;
		public static final int UnrecognizedPDUParameter = 4;
		public static final int UnexpectedPDUParameter = 5;
		public static final int InvalidPDUParameterValue = 6;
	}
	private int reason;
	public int getReason()
	{
		return this.reason;
	}

	public A_ABORT_PDU(int pduType, long pduLength, 
			int source, int reason)
	{
		super(pduType, pduLength);
		this.source = source;
		this.reason = reason;
	}
}
