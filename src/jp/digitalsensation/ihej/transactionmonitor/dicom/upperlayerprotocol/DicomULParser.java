package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import java.lang.Iterable;
import java.util.ArrayList;
import static jp.digitalsensation.ihej.transactionmonitor.utils.ByteArrayUtility.*;
import static jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol.ProtocolDataUnitTypes.*;
import static jp.digitalsensation.ihej.transactionmonitor.utils.ObjectDump.*;

public class DicomULParser {
	public static long DefaultMaximumPDULength = Integer.MAX_VALUE;

	private long maximumPDULength = DefaultMaximumPDULength;
	public long getMaximumPDULength()
	{
		return this.maximumPDULength;
	}
	public void setMaximumPDULength(long value)
	{
		this.maximumPDULength = value;
	}

	private byte[] buffer = new byte[0];
	
	public Iterable<ProtocolDataUnit> push(byte[] data)
		throws DicomULParserException
	{
		int offset = 0;
		ArrayList<ProtocolDataUnit> results = new ArrayList<ProtocolDataUnit>();

		this.buffer = concatinateBytes(this.buffer, data);

		while (true)
		{
			if (this.buffer.length - offset < 6)
			{
				break;
			}
			
			int pduType = getUnsignedByte(this.buffer, offset + 0);
			if (!(A_ASSOCIATE_RQ <= pduType && pduType <= A_ABORT))
			{
				throw new DicomULParserException(
					"Unrecognized PDU type found : 0x" + Integer.toHexString(pduType));
			}
			
			long pduLength = getUnsignedIntegerBigEndian(this.buffer, offset + 2);
			long totalPDULength = pduLength + 6;
			if (!(10 <= totalPDULength && totalPDULength + 6 < this.maximumPDULength))
			{
				throw new DicomULParserException(
					"PDU-length is not in valid range : " + Long.toString(totalPDULength));
			}
			
			if (this.buffer.length - offset < totalPDULength)
			{
				break;
			}
			
			ProtocolDataUnit pdu = ProtocolDataUnit.parse(this.buffer, offset);
			offset += totalPDULength;
			results.add(pdu);
		}
		
		this.buffer = dropBytes(this.buffer, 0, offset);
		return results;
	}
	
	public static void main(String[] args)
	{
		for (String arg : args)
		{
			try
			{
				byte[] buffer = new byte[8192];
				java.io.FileInputStream fis = new java.io.FileInputStream(arg);
				DicomULParser p = new DicomULParser();
				
				int offset = 0;
				while (true)
				{
					int read = fis.read(buffer, offset, 8192);
					if (read == -1)
					{
						break;
					}
					
					byte[] data = new byte[read];
					System.arraycopy(buffer, 0, data, 0, read);
					Iterable<ProtocolDataUnit> pdus = p.push(data);
					
					for (ProtocolDataUnit pdu : pdus)
					{
						dump(pdu);
						System.out.println();
					}
				};
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
}
