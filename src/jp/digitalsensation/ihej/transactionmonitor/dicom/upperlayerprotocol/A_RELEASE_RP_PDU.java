package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class A_RELEASE_RP_PDU extends ProtocolDataUnit
{
	public A_RELEASE_RP_PDU(int pduType, long pduLength)
	{
		super(pduType, pduLength);
	}
}
