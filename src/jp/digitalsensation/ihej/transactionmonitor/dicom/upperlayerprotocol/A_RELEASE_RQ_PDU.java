package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class A_RELEASE_RQ_PDU extends ProtocolDataUnit
{
	public A_RELEASE_RQ_PDU(int pduType, long pduLength)
	{
		super(pduType, pduLength);
	}
}
