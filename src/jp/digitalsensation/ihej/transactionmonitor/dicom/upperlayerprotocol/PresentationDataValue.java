package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import static jp.digitalsensation.ihej.transactionmonitor.utils.ByteArrayUtility.*;

public class PresentationDataValue
{
	private int messageControlHeader;
	public int getMessageControlHeader()
	{
		return this.messageControlHeader;
	}
	public boolean isCommandFragment()
	{
		return (this.messageControlHeader & 0x01) != 0;
	}
	public boolean isLastFragment()
	{
		return (this.messageControlHeader & 0x02) != 0;
	}
	
	private byte[] data;
	public byte[] getData()
	{
		return this.data.clone();
	}
	
	public PresentationDataValue(int messageControlHeader, byte[] data)
	{
		this.messageControlHeader = messageControlHeader;
		this.data = data.clone();
	}
	
	public static PresentationDataValue parse(byte[] buffer, int offset, long itemLength)
		throws DicomULParserException
	{
		if (buffer.length - offset < itemLength)
		{
			throw new DicomULParserException(
					"");
		}
		
		int mch = getUnsignedByte(buffer, offset);
		byte[] data = getBytes(buffer, offset + 1, (int)(itemLength - 1));
		
		return new PresentationDataValue(mch, data);
	}
}
