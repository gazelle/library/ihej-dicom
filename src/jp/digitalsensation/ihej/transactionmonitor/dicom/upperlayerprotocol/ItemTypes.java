package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public final class ItemTypes {
	public static final int ApplicationContext = 0x10;
	public static final int PresentationContext = 0x20;
	public static final int PresentationContextResult = 0x21;
	public static final int AbstractSyntax = 0x30;
	public static final int TransferSyntax = 0x40;
	public static final int UserInformation = 0x50;
	public static final int MaximumLength = 0x51;
	public static final int ImplementationClassUID = 0x52;
	public static final int AsynchronousOperationsWindow = 0x53;
	public static final int SCUOrSCPRoleSelection = 0x54;
	public static final int ImplementationVersionName = 0x55;
	public static final int SOPClassExtendedNegotiation = 0x56;
	// TODO: some more items are defined on DICOM2009
}
