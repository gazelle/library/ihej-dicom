package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class ApplicationContextItem extends Item
{
	private String applicationContextName;
	public String getApplicationContextName()
	{
		return this.applicationContextName;
	}
	
	public ApplicationContextItem(int itemType, int itemLength, String applicationContextName)
	{
		super(itemType, itemLength);
		this.applicationContextName = applicationContextName;
	}
}
