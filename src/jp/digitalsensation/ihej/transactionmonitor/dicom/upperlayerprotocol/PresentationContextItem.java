package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import java.util.List;
import java.util.Collections;

public class PresentationContextItem extends Item
{
	private short presentationContextID;
	public short getPresentationContextID()
	{
		return this.presentationContextID;
	}
	
	private List<Item> subItems;
	public List<Item> getSubItems()
	{
		return Collections.unmodifiableList(this.subItems);
	}
	
	public PresentationContextItem(int itemType, int itemLength, short presentationContextID, List<Item> subItems)
	{
		super(itemType, itemLength);
		this.presentationContextID = presentationContextID;
		this.subItems = subItems;
	}

}
