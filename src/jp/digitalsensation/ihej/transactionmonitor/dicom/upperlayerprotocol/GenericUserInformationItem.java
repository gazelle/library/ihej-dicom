package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class GenericUserInformationItem extends Item
{
	private byte[] data;
	public byte[] getData()
	{
		return this.data.clone();
	}
	
	public GenericUserInformationItem(int itemType, int itemLength, byte[] data)
	{
		super(itemType, itemLength);
		this.data = data.clone();
	}

}
