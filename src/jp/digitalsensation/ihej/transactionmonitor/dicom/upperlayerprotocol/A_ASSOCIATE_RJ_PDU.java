package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class A_ASSOCIATE_RJ_PDU extends ProtocolDataUnit
{
	public static class Result
	{
		public static final int RejectedParmanent = 1;
		public static final int RejectedTransient = 2;
	}
	private int result;

	public int getResult()
	{
		return this.result;
	}
	
	public static class Source
	{
		public static final int DICOMULServiceUesr = 1;
		public static final int DICOMULServiceProvider_ACSERelatedFunction = 2;
		public static final int DICOMULServiceProvider_PresentationRelatedFunction = 3;
	}
	private int source;
	public int getSource()
	{
		return this.source;
	}
	
	public static class Reason
	{
		// when source == "DICOM UL service-user"
		public static final int NoReasonGiven = 1;
		public static final int ApplicationContextNameNotSupported = 2;
		public static final int CallingAETitleNotRecognized = 3;
		public static final int CallingAEQualifierNotRecognized = 4;
		public static final int CallingAPInvocationIdentifierNotRecognized = 5;
		public static final int CallingAEInvocationIdentifierNotRecognized = 6;
		public static final int CalledAETitleNotRecognized = 7;
		public static final int CalledAEQualifierNotRecognized = 8;
		public static final int CalledAPInvocationIdentifierNotRecognized = 9;
		public static final int CalledAEInvocationIdentifierNotRecognized = 10;
		
		// when source == "DICOM UL service provided (ACSE related function)"
		// public static final int NoReasonGiven = 1;
		public static final int ProtocolVersionNotSupported = 2;
		
		//when source == "DICOM UL service provided (Presentation related function)"
		public static final int TemporaryCongestion = 1;
		public static final int LocalLimitExceeded = 2;
		public static final int CalledAddressUnknown = 3;
		public static final int PresentationProtocolVersionNotSupported = 4;
		public static final int NoServiceAccessPointAvailable = 5;
	}
	private int reason;
	public int getReason()
	{
		return this.reason;
	}

	public A_ASSOCIATE_RJ_PDU(int pduType, long pduLength, int result,
			int source, int reason)
	{
		super(pduType, pduLength);
		this.result = result;
		this.source = source;
		this.reason = reason;
	}
}
