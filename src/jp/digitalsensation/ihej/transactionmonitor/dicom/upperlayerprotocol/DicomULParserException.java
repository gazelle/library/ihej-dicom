package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import jp.digitalsensation.ihej.transactionmonitor.dicom.DicomParserException;

@SuppressWarnings("serial")
public class DicomULParserException extends DicomParserException
{

	public DicomULParserException()
	{
	}

	public DicomULParserException(String arg0)
	{
		super(arg0);
	}

	public DicomULParserException(Throwable arg0)
	{
		super(arg0);
	}

	public DicomULParserException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

}
