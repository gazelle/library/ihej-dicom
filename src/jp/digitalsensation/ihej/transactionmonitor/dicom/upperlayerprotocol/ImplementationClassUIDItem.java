package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class ImplementationClassUIDItem extends Item
{
	private String implementationClassUID;
	public String getImplementationClassUID()
	{
		return this.implementationClassUID;
	}
	
	public ImplementationClassUIDItem(int itemType, int itemLength, String implementationClassUID)
	{
		super(itemType, itemLength);
		this.implementationClassUID = implementationClassUID;
	}

}
