package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import java.util.ArrayList;
import java.util.List;

import static jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol.ItemTypes.*;
import static jp.digitalsensation.ihej.transactionmonitor.utils.ByteArrayUtility.*;

public abstract class Item
{
	protected int itemType;
	public int getItemType()
	{
		return this.itemType;
	}
	
	protected int itemLength;
	public int getItemLength()
	{
		return this.itemLength;
	}
	public int getTotalLength()
	{
		return this.itemLength + 4;
	}
	
	protected Item(int itemType, int itemLength)
	{
		this.itemType = itemType;
		this.itemLength = itemLength;
	}

	public static Item parse(byte[] buffer, int offset)
		throws DicomULParserException
	{
		if (buffer.length - offset < 4)
		{
			throw new DicomULParserException(
					"buffer length is not enough to parse DICOM UL Item.");
		}
		
		int itemType = getUnsignedByte(buffer, offset + 0);
		int itemLength = getUnsignedShortBigEndian(buffer, offset + 2);
		
		switch (itemType)
		{
		case ApplicationContext:
		case AbstractSyntax:
		case TransferSyntax:
		case ImplementationClassUID:
		case ImplementationVersionName:
			String str = getString(buffer, offset + 4, itemLength);
			switch (itemType)
			{
			case ApplicationContext:
				return new ApplicationContextItem(itemType, itemLength, str);
			case AbstractSyntax:
				return new AbstractSyntaxItem(itemType, itemLength, str);
			case TransferSyntax:
				return new TransferSyntaxItem(itemType, itemLength, str);
			case ImplementationClassUID:
				return new ImplementationClassUIDItem(itemType, itemLength, str);
			case ImplementationVersionName:
				return new ImplementationVersionNameItem(itemType, itemLength, str);
			default:
				throw new DicomULParserException(
						"Unrecognized item type found : 0x" + Integer.toHexString(itemType));
			}
			
		case PresentationContext:
		case PresentationContextResult:
			if (itemLength < 4)
			{
				throw new DicomULParserException(
				"buffer length is not enough to parse DICOM UL Presentation-Context Item.");
			}
			short presentationContextID = getUnsignedByte(buffer, offset + 4);
			int result = getUnsignedByte(buffer, offset + 6);
			List<Item> items = parseSubItems(buffer, offset + 8, itemLength - 4);
			
			switch (itemType)
			{
			case PresentationContext:
				return new PresentationContextItem(itemType, itemLength, presentationContextID, items);
			case PresentationContextResult:
				return new PresentationContextResultItem(itemType, itemLength, presentationContextID, result, items);
			default:
				throw new DicomULParserException(
						"Unrecognized item type found : 0x" + Integer.toHexString(itemType));
			}
			
		case UserInformation:
			List<Item> userData = parseSubItems(buffer, offset + 4, itemLength);
			return new UserInformationItem(itemType, itemLength, userData);
			
		case MaximumLength:
			if (itemLength != 4)
			{
				throw new DicomULParserException(
					"buffer length is not enough to parse DICOM UL MaximumLengthUserItem.");
			}
			long maximumLengthReceived = getUnsignedIntegerBigEndian(buffer, offset + 4);
			return new MaximumLengthItem(itemType, itemLength, maximumLengthReceived);
			
		case AsynchronousOperationsWindow:
			if (itemLength != 4)
			{
				throw new DicomULParserException(
				"buffer length is not enough to parse DICOM UL AsynchronousOperationWindowUserItem.");
			}
			int maximumNumberOperationsInvoked = getUnsignedShortBigEndian(buffer, offset + 4);
			int maximumNumberOperationsPerformed = getUnsignedShortBigEndian(buffer, offset + 6);
			return new AsynchronousOperationsWindowItem(itemType, itemLength, maximumNumberOperationsInvoked, maximumNumberOperationsPerformed);
			
		case SCUOrSCPRoleSelection:
			if (itemLength < 4)
			{
				throw new DicomULParserException(
				"buffer length is not enough to parse DICOM UL Item.");
			}
			int sopClassUIDLength = getUnsignedShortBigEndian(buffer, offset + 4);
			if (sopClassUIDLength != itemLength - 4)
			{
				throw new DicomULParserException(
						"SOP Class UID length in SCU/SCP Role Selection Item is not valid");
			}
			String sopClassUID = getString(buffer, offset + 6, sopClassUIDLength);
			int scuRole = getUnsignedByte(buffer, offset + 6 + sopClassUIDLength);
			int scpRole = getUnsignedByte(buffer, offset + 6 + sopClassUIDLength + 1);
			
			return new SCUOrSCPRoleSelectionItem(itemType, itemLength, sopClassUIDLength, sopClassUID, scuRole, scpRole);
			
		case SOPClassExtendedNegotiation:
			if (itemLength < 2)
			{
				throw new DicomULParserException(
				"buffer length is not enough to parse DICOM UL Item.");
			}
			int sopClassUIDLength2 = getUnsignedShortBigEndian(buffer, offset + 4);
			if (sopClassUIDLength2 > itemLength - 2)
			{
				throw new DicomULParserException(
						"SOP Class UID length in SOP Class Extended Negotiation User Item is not valid");
			}
			String sopClassUID2 = getString(buffer, offset + 6, sopClassUIDLength2);
			byte[] serviceClassApplicationInformation =
				getBytes(buffer, offset + 6 + sopClassUIDLength2, itemLength - 2 - sopClassUIDLength2);
			
			return new SOPClassExtendedNegotiationItem(itemType, itemLength, sopClassUIDLength2, sopClassUID2, serviceClassApplicationInformation);
		
		default:
			if (UserInformation < itemType && itemType <= 0xFF)
			{
				byte[] data = getBytes(buffer, offset + 4, itemLength);
				return new GenericUserInformationItem(itemType, itemLength, data);
			}
			else
			{
				throw new DicomULParserException(
						"Unrecognized item type found : 0x" + Integer.toHexString(itemType));
			}
		}
	}
	
	private static List<Item> parseSubItems(byte[] buffer, int offset, int length)
		throws DicomULParserException
	{
		List<Item> items = new ArrayList<Item>();
		
		int itemOffset = 0;
		while (itemOffset < length)
		{
			Item item = Item.parse(buffer, offset + itemOffset);
			items.add(item);
			itemOffset += item.getTotalLength();
		}
		
		return items;
	}
}
