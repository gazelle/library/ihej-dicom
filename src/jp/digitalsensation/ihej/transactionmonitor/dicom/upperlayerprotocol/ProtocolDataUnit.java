package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import static jp.digitalsensation.ihej.transactionmonitor.utils.ByteArrayUtility.*;
import static jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol.ProtocolDataUnitTypes.*;

import java.util.ArrayList;

public abstract class ProtocolDataUnit
{
	private int pduType;
	public int getPDUType()
	{
		return this.pduType;
	}
	
	private long pduLength;
	public long getPDULength()
	{
		return this.pduLength;
	}
	
	public long getTotalLength()
	{
		return this.pduLength + 6;
	}
	
	protected ProtocolDataUnit(int pduType, long pduLength)
	{
		this.pduType = pduType;
		this.pduLength = pduLength;
	}
	
	public static ProtocolDataUnit parse(byte[] buffer, int offset)
		throws DicomULParserException
	{
		if (buffer.length - offset < 6 )
		{
			throw new DicomULParserException(
				"buffer length is not enough to parse DICOM Protocol Data Unit.");
		}
		
		int pduType = getUnsignedByte(buffer, offset + 0);
		long pduLength = getUnsignedIntegerBigEndian(buffer, offset + 2);
		
		if (buffer.length - offset < pduLength)
		{
			throw new DicomULParserException(
					"PDU-Length exceeds buffer length.");
		}
		
		switch (pduType)
		{
		case A_ASSOCIATE_RQ:
		case A_ASSOCIATE_AC:
			if (pduLength + 6 < 74)
			{
				throw new DicomULParserException(
						"buffer length is not enough to parse DICOM A-ASSOCIATE-RQ or AC PDU.");
			}
			int protocolVersion = getUnsignedShortBigEndian(buffer, 6);
			String calledEntityTitle = getString(buffer, 10, 16).trim();
			String callingEntityTitle = getString(buffer, 26, 16).trim();
			ArrayList<Item> variableItems = parseItems(buffer, offset + 74, (int)(pduLength - 68));
			
			switch (pduType)
			{
			case A_ASSOCIATE_RQ:
				return new A_ASSOCIATE_RQ_PDU(pduType, pduLength, protocolVersion, calledEntityTitle, callingEntityTitle, variableItems);
			case A_ASSOCIATE_AC:
				return new A_ASSOCIATE_AC_PDU(pduType, pduLength, protocolVersion, calledEntityTitle, callingEntityTitle, variableItems);
			default:
				throw new DicomULParserException(
					"Unrecognized PDU type found : 0x" + Integer.toHexString(pduType));
			}
			
		case A_ASSOCIATE_RJ:
		case A_RELEASE_RQ:
		case A_RELEASE_RP:
		case A_ABORT:
			if (pduLength + 6 != 10)
			{
				throw new DicomULParserException(
					"PDU-length is not valid for the PDU");
			}
			
			int result = getUnsignedByte(buffer, offset + 7);
			int source = getUnsignedByte(buffer, offset + 8);
			int reason = getUnsignedByte(buffer, offset + 9);
			
			switch (pduType)
			{
			case A_ASSOCIATE_RJ:
				return new A_ASSOCIATE_RJ_PDU(pduType, pduLength, result, source, reason);
			case A_RELEASE_RQ:
				return new A_RELEASE_RQ_PDU(pduType, pduLength);
			case A_RELEASE_RP:
				return new A_RELEASE_RP_PDU(pduType, pduLength);
			case A_ABORT:
				return new A_ABORT_PDU(pduType, pduLength, source, reason);
			default:
				throw new DicomULParserException(
						"Unrecognized PDU type found : 0x" + Integer.toHexString(pduType));
			}
			
		case P_DATA_TF:
			ArrayList<PresentationDataValueItem> presentationDataValueItems =
				parsePresentationDataValueItems(buffer, offset + 6, (int)pduLength);
			return new P_DATA_TF_PDU(pduType, pduLength, presentationDataValueItems);
			
		default:
				throw new DicomULParserException(
					"Unrecognized PDU type found : 0x" + Integer.toHexString(pduType));
		}
	}
	
	private static ArrayList<Item> parseItems(byte[] buffer, int offset, int length)
		throws DicomULParserException
	{
		ArrayList<Item> items = new ArrayList<Item>();
		
		int itemOffset = 0;
		while (itemOffset < length)
		{
			Item item = Item.parse(buffer, offset + itemOffset);
			items.add(item);
			itemOffset += item.getTotalLength();
		}
		
		return items;		
	}
	
	private static ArrayList<PresentationDataValueItem> parsePresentationDataValueItems(byte[] buffer, int offset, int length)
		throws DicomULParserException
	{
		ArrayList<PresentationDataValueItem> items = new ArrayList<PresentationDataValueItem>();
		
		int itemOffset = 0;
		while (itemOffset < length)
		{
			PresentationDataValueItem item = PresentationDataValueItem.parse(buffer, offset + itemOffset);
			items.add(item);
			itemOffset += item.getTotalLength();
		}
		
		return items;		
	}
}
