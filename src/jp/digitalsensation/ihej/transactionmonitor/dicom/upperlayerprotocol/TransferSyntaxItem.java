package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class TransferSyntaxItem extends Item
{
	private String transferSyntax;
	public String getTransferSyntax()
	{
		return this.transferSyntax;
	}
	
	public TransferSyntaxItem(int itemType, int itemLength, String transferSyntax)
	{
		super(itemType, itemLength);
		this.transferSyntax = transferSyntax;
	}
}
