package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import java.util.List;
import java.util.Collections;

public class PresentationContextResultItem extends Item
{
	public class Result
	{
		public static final int Acceptance = 0;
		public static final int UserRejection = 1;
		public static final int NoReason = 2;
		public static final int AbstractSyntaxNotSupported = 3;
		public static final int TransferSyntaxNotSupported = 4;
	}
	
	private short presentationContextID;
	public short getPresentationContextID()
	{
		return this.presentationContextID;
	}
	
	private int result;
	public int getResult()
	{
		return this.result;
	}
	
	private List<Item> subItems;
	public List<Item> getSubItems()
	{
		return Collections.unmodifiableList(this.subItems);
	}
	
	public PresentationContextResultItem(int itemType, int itemLength, short presentationContextID, int result, List<Item> subItems)
	{
		super(itemType, itemLength);
		this.presentationContextID = presentationContextID;
		this.result = result;
		this.subItems = subItems;
	}

}
