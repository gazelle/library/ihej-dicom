package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class MaximumLengthItem extends Item
{
	private long maximumLengthReceived;
	public long getMaximumLengthReceived()
	{
		return this.maximumLengthReceived;
	}

	public MaximumLengthItem(int itemType, int itemLength, long maximumLengthReceived)
	{
		super(itemType, itemLength);
		this.maximumLengthReceived = maximumLengthReceived;
	}

}
