package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

public class AbstractSyntaxItem extends Item
{
	private String abstractSyntax;
	public String getAbstractSyntax()
	{
		return this.abstractSyntax;
	}
	
	public AbstractSyntaxItem(int itemType, int itemLength, String abstractSyntax)
	{
		super(itemType, itemLength);
		this.abstractSyntax = abstractSyntax;
	}
}
