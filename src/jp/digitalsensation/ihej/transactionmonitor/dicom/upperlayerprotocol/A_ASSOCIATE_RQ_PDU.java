package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import java.util.List;
import java.util.Collections;

public class A_ASSOCIATE_RQ_PDU extends ProtocolDataUnit
{
	private int protocolVersion;
	public int getProtocolVersion()
	{
		return this.protocolVersion;
	}
	
	private String calledEntityTitle;
	public String getCalledEntityTitle()
	{
		return this.calledEntityTitle;
	}
	
	private String callingEntityTitle;
	public String getCallingEntityTitle()
	{
		return this.callingEntityTitle;
	}
	
	private List<Item> variableItems;
	public List<Item> getVariableItems()
	{
		return Collections.unmodifiableList(this.variableItems);
	}
	
	public A_ASSOCIATE_RQ_PDU(int pduType, long pduLength, int protocolVersion, String calledEntityTitle, String callingEntityTitle, List<Item> variableItems)
	{
		super(pduType, pduLength);
		this.protocolVersion = protocolVersion;
		this.calledEntityTitle = calledEntityTitle;
		this.callingEntityTitle = callingEntityTitle;
		this.variableItems = variableItems;
	}
}
