package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import static jp.digitalsensation.ihej.transactionmonitor.utils.ByteArrayUtility.*;

public class PresentationDataValueItem
{
	private long itemLength;
	public long getItemLength()
	{
		return this.itemLength;
	}
	public long getTotalLength()
	{
		return this.itemLength + 4;
	}
	
	private short presentationContextID;
	public short getPresentationContextID()
	{
		return this.presentationContextID;
	}
	
	private PresentationDataValue presentationDataValue;
	public PresentationDataValue getPresentationDataValue()
	{
		return this.presentationDataValue;
	}
	
	public PresentationDataValueItem(long itemLength, short presentationContextID, PresentationDataValue presentationDataValue)
	{
		this.itemLength = itemLength;
		this.presentationContextID = presentationContextID;
		this.presentationDataValue = presentationDataValue;
	}

	public static PresentationDataValueItem parse(byte[] buffer, int offset)
		throws DicomULParserException
	{
		if (buffer.length - offset < 5)
		{
			throw new DicomULParserException(
					"buffer length is not enough to parse Presentation-Data-Value Item");
		}
		
		long itemLength = getUnsignedIntegerBigEndian(buffer, offset + 0);
		if (buffer.length - offset < itemLength)
		{
			throw new DicomULParserException(
					"Item length in Presentation-Data-Value Item exceeds buffer length");
		}
		
		short presentationContextID = getUnsignedByte(buffer, offset + 4);
		
		PresentationDataValue pdv = PresentationDataValue.parse(buffer, offset + 5, itemLength - 1);
		
		return new PresentationDataValueItem(itemLength, presentationContextID, pdv);
	}

}
