package jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol;

import java.util.List;
import java.util.Collections;

public class P_DATA_TF_PDU extends ProtocolDataUnit {
	private List<PresentationDataValueItem> presentationDataValueItems;
	public List<PresentationDataValueItem> getPresentationDataValueItems()
	{
		return Collections.unmodifiableList(this.presentationDataValueItems);
	}
	
	public P_DATA_TF_PDU(int pduType, long pduLength, List<PresentationDataValueItem> presentationDataValueItems)
	{
		super(pduType, pduLength);
		this.presentationDataValueItems = presentationDataValueItems;
	}
}
