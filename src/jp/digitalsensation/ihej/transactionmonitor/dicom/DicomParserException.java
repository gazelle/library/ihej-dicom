package jp.digitalsensation.ihej.transactionmonitor.dicom;



@SuppressWarnings("serial")
public class DicomParserException extends Exception
{

	public DicomParserException()
	{
		super("Dicom Parser Failed.");
	}

	public DicomParserException(String arg0)
	{
		super("Dicom Parser Failed : " + arg0);
	}

	public DicomParserException(Throwable arg0)
{
		super("Dicom Parser Failed.", arg0);
	}

	public DicomParserException(String arg0, Throwable arg1)
{
		super("Dicom Parser Failed : " + arg0, arg1);
	}

}
