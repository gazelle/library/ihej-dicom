package jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange;

import jp.digitalsensation.ihej.transactionmonitor.dicom.DicomParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static jp.digitalsensation.ihej.transactionmonitor.utils.ByteArrayUtility.getUnsignedIntegerLittleEndian;
import static jp.digitalsensation.ihej.transactionmonitor.utils.ByteArrayUtility.getUnsignedShortLittleEndian;

public class CommandSetReader
{
	private final InputStream in;
	
	public CommandSetReader(InputStream in)
	{
		this.in = in;
	}
	
	public class Attribute
	{
		private final int groupNumber;
		public int getGroupNumber()
		{
			return this.groupNumber;
		}
		
		private final int elementNumber;
		public int getElementNumber()
		{
			return this.elementNumber;
		}
		
		private final long valueLength;
		public long getValueLength()
		{
			return this.valueLength;
		}
		
		private final byte[] value;
		public byte[] getValue()
		{
			return this.value.clone();
		}
		
		public Attribute(int groupNumber, int elementNumber, long length, byte[] value)
		{
			this.groupNumber = groupNumber;
			this.elementNumber = elementNumber;
			this.valueLength = length;
			this.value = value.clone();
		}
	}
	
	private Attribute currentAttribute = null;
	
	public boolean next()
		throws DicomParserException, IOException
	{
		this.currentAttribute = null;
		
		byte[] header;
		{
			int offset = 0;
			header = new byte[8];

			{
				int read = this.in.read(header, offset, header.length - offset);
				if (read == -1)
				{
					return false;
				}
				offset += read;
			}
			
			while (offset < header.length)
			{
				int read = this.in.read(header, offset, header.length - offset);
				if (read == -1)
				{
					throw new DicomParserException(
							"Unexpected eof found when reading command set header.");
				}
				offset += read;
			}
		}
		
		int groupNumber = getUnsignedShortLittleEndian(header, 0);
		int elementNumber = getUnsignedShortLittleEndian(header, 2);
		long valueLength = getUnsignedIntegerLittleEndian(header, 4);
		if (0xFFFFFFFFL <= valueLength )
		{
			throw new DicomParserException(
					"Unknown length attribute found in command set.");
		}
		else if (0x8FFFFFFFL <= valueLength)
		{
			throw new DicomParserException(
					"Too large commandset attribute found in command set.");
		}
		
		byte[] value;
		try
		{
			value = new byte[(int)valueLength];
		}
		catch (OutOfMemoryError ex)
		{
			throw new DicomParserException(
					"Too large commandset attribute found in command set.", ex);
		}
		
		{
			int offset = 0;
			while (offset < value.length)
			{
				int read = this.in.read(value, offset, value.length - offset);
				if (read == -1)
				{
					throw new DicomParserException(
						"Unexpected eof found when reading command set header.");
				}
				offset += read;
			}
		}
		
		this.currentAttribute = new Attribute(
				groupNumber,
				elementNumber,
				valueLength,
				value);
		return true;
	}
	
	public Attribute getAttribute()
		throws IllegalAccessError
	{
		if (this.currentAttribute == null)
		{
			throw new IllegalAccessError();
		}
		
		return this.currentAttribute;
	}
	
	public boolean moveTo(int group, int element)
		throws DicomParserException, IOException
	{
		while (this.next())
		{
			if (this.currentAttribute.groupNumber == group &&
					this.currentAttribute.elementNumber == element)
			{
				return true;
			}
		}
		return false;
	}
	
	public int getUSValue()
		throws IllegalAccessError, ClassCastException
	{
		if (this.currentAttribute == null)
		{
			throw new IllegalAccessError();
		}
		
		if (this.currentAttribute.value == null ||
				this.currentAttribute.value.length < 2)
		{
			throw new ClassCastException();
		}
		
		return getUnsignedShortLittleEndian(this.currentAttribute.value, 0);
	}
	
	public Iterable<Integer> getUSValues()
		throws IllegalAccessError, ClassCastException
	{
		if (this.currentAttribute == null)
		{
			throw new IllegalAccessError();
		}
		
		if (this.currentAttribute.value == null)
		{
			throw new ClassCastException();
		}
		
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (int i = 0; i < this.currentAttribute.value.length; i += 2)
		{
			result.add(
					getUnsignedShortLittleEndian(this.currentAttribute.value, i));
		}
		
		return result;
	}
}
