package jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange;


import java.util.Map;
import java.util.HashMap;

public class Association
{
	private String callingApplicationEntityTitle;
	public String getCallingApplicationEntityTitle()
	{
		return this.callingApplicationEntityTitle;
	}
	public void setCallingApplicationEntityTitle(String value)
	{
		this.callingApplicationEntityTitle = value;
	}
	
	private String callingApplicationEntityImplementationClassUid;
	public String getCallingApplicationEntityImplementationClassUid()
	{
		return this.callingApplicationEntityImplementationClassUid;
	}
	public void setCallingApplicationEntityImplementationClassUid(String value)
	{
		this.callingApplicationEntityImplementationClassUid = value;
	}
	
	private String callingApplicationEntityImplementationVersion;
	public String getCallingApplicationEntityImplementationVersion()
	{
		return this.callingApplicationEntityImplementationVersion;
	}
	public void setCallingApplicationEntityImplementationVersion(String value)
	{
		this.callingApplicationEntityImplementationVersion = value;
	}
	
	private String calledApplicationEntityTitle;
	public String getCalledApplicationEntityTitle()
	{
		return this.calledApplicationEntityTitle;
	}
	public void setCalledApplicationEntityTitle(String value)
	{
		this.calledApplicationEntityTitle = value;
	}
	
	private String calledApplicationEntityImplementationClassUid;
	public String getCalledApplicationEntityImplementationClassUid()
	{
		return this.calledApplicationEntityImplementationClassUid;
	}
	public void setCalledApplicationEntityImplementationClassUid(String value)
	{
		this.calledApplicationEntityImplementationClassUid = value;
	}
	
	private String calledApplicationEntityImplementationVersion;
	public String getCalledApplicationEntityImplementationVersion()
	{
		return this.calledApplicationEntityImplementationVersion;
	}
	public void setCalledApplicationEntityImplementationVersion(String value) {
		this.calledApplicationEntityImplementationVersion = value;
	}
	
	private final HashMap<Short, PresentationContext> presentationContexts;
	public Map<Short, PresentationContext> getPresentationContexts()
	{
		return this.presentationContexts;
	}
	
	public Association()
	{
		this.callingApplicationEntityTitle = null;
		this.callingApplicationEntityImplementationClassUid = null;
		this.callingApplicationEntityImplementationClassUid = null;
		this.calledApplicationEntityTitle = null;
		this.calledApplicationEntityImplementationClassUid = null;
		this.calledApplicationEntityImplementationVersion = null;
		this.presentationContexts = new HashMap<Short, PresentationContext>();
	}
}
