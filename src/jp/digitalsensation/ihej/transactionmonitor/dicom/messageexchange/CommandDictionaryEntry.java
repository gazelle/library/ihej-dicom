package jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange;

import jp.digitalsensation.ihej.transactionmonitor.dicom.datastructure.Tag;
import jp.digitalsensation.ihej.transactionmonitor.dicom.datastructure.ValueRepresentation;

public class CommandDictionaryEntry
{	
	private final String messageField;
	public String getMessageField()
	{
		return this.messageField;
	}
	
	private final String keyword;
	public String getKeyword()
	{
		return this.keyword;
	}
	
	private final Tag tag;
	public Tag getTag()
	{
		return this.tag;
	}
	
	private final ValueRepresentation valueRepresentation;
	public ValueRepresentation getValueRepresentation()
	{
		return this.valueRepresentation;
	}
	
	private final String valueMultiplicity;
	public String getValueMultiplicity()
	{
		return this.valueMultiplicity;
	}
	
	private final String descriptionOfField;
	public String getDescriptionOfField()
	{
		return this.descriptionOfField;
	}
	
	private final boolean retired;
	public boolean getRetired()
	{
		return this.retired;
	}
	
	public CommandDictionaryEntry(
			String messageField,
			String keyword,
			Tag tag,
			ValueRepresentation valueRepresentation,
			String valueMultiplicity,
			String descriptionOfField,
			boolean retired)
	{
		this.messageField = messageField;
		this.keyword = keyword;
		this.tag = tag;
		this.valueRepresentation = valueRepresentation;
		this.valueMultiplicity = valueMultiplicity;
		this.descriptionOfField = descriptionOfField;
		this.retired = retired;
	}
}
