package jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange;

import java.util.HashMap;
import jp.digitalsensation.ihej.transactionmonitor.dicom.datastructure.Tag;
import jp.digitalsensation.ihej.transactionmonitor.dicom.datastructure.ValueRepresentation;

public class CommandDictionary extends HashMap<Tag, CommandDictionaryEntry>
{
	private static final long serialVersionUID = -8121857775319877845L;
	
	private static CommandDictionary defaultDictionary = new CommandDictionary();
	public static CommandDictionary getDefault()
	{
		return CommandDictionary.defaultDictionary;
	}
	public static void setDefault(CommandDictionary value)
	{
		CommandDictionary.defaultDictionary = value;
	}
	
	static
	{
		{
			Tag tag = new Tag(0x0000, 0x0000);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Command Group Length",
					"CommandGroupLength",
					tag,
					ValueRepresentation.UL,
					"1",
					"The even number of bytes from the end of the value field to the beginning of the next group.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0002);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Affected SOP Class UID",
					"AffectedSOPClassUID",
					tag,
					ValueRepresentation.UI,
					"1",
					"The affected SOP Class UID associated with the operation.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0003);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Requested SOP Class UID",
					"RequestedSOPClassUID",
					tag,
					ValueRepresentation.UI,
					"1",
					"The requested SOP Class UID associated with the operation.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0100);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Command Field",
					"CommandField",
					tag,
					ValueRepresentation.US,
					"1",
					"This field distinguishes the DIMSE operation conveyed by this Message. This field shall be set to one of the following values:\n" + 
					"0001H C-STORE-RQ\n" + 
					"8001H C-STORE-RSP\n" + 
					"0010H C-GET-RQ\n" + 
					"8010H C-GET-RSP\n" + 
					"0020H C-FIND-RQ\n" + 
					"8020H C-FIND-RSP\n" + 
					"0021H C-MOVE-RQ\n" + 
					"8021H C-MOVE-RSP\n" + 
					"0030H C-ECHO-RQ\n" + 
					"8030H C-ECHO-RSP\n" + 
					"0100H N-EVENT-REPORT-RQ\n" + 
					"8100H N-EVENT-REPORT-RSP\n" + 
					"0110H N-GET-RQ\n" + 
					"8110H N-GET-RSP\n" + 
					"0120H N-SET-RQ\n" + 
					"8120H N-SET-RSP\n" + 
					"0130H N-ACTION-RQ\n" + 
					"8130H N-ACTION-RSP\n" + 
					"0140H N-CREATE-RQ\n" + 
					"8140H N-CREATE-RSP\n" + 
					"0150H N-DELETE-RQ\n" + 
					"8150H N-DELETE-RSP\n" + 
					"0FFFH C-CANCEL-RQ",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0110);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Message ID",
					"MessageID",
					tag,
					ValueRepresentation.US,
					"1",
					"Implementation-specific value that distinguishes this Message from other Messages.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0120);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Message ID Being Responded To",
					"MessageIDBeingRespondedTo",
					tag,
					ValueRepresentation.US,
					"1",
					"Shall be set to the value of the Message ID (0000,0110) field used in associated request Message.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0600);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Move Destination",
					"MoveDestination",
					tag,
					ValueRepresentation.AE,
					"1",
					"Shall be set to the DICOM AE Title of the destination DICOM AE to which the C-STORE sub-operations are being performed.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0700);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Priority",
					"Priority",
					tag,
					ValueRepresentation.US,
					"1",
					"The priority shall be set to one of the following values:\n" + 
					"LOW = 0002H\n" + 
					"MEDIUM = 0000H\n" + 
					"HIGH = 0001H",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0800);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Data Set Type",
					"CommandDataSetType",
					tag,
					ValueRepresentation.US,
					"1",
					"This field indicates if a Data Set is present in the Message. This field shall be set to the value of 0101H if no Data Set is present; any other value indicates a Data Set is included in the Message.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0900);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Status",
					"Status",
					tag,
					ValueRepresentation.US,
					"1",
					"Confirmation status of the operation. See Annex C.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0901);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Offending Element",
					"OffendingElement",
					tag,
					ValueRepresentation.AT,
					"1-n",
					"If status is Cxxx, then this field contains a list of the elements in which the error was detected.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0902);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Error Comment",
					"ErrorComment",
					tag,
					ValueRepresentation.LO,
					"1",
					"This field contains an application-specific text description of the error detected.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0903);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Error ID",
					"ErrorID",
					tag,
					ValueRepresentation.US,
					"1",
					"This field shall optionally contain an application-specific error code.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1000);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Affected SOP Instance UID",
					"AffectedSOPInstanceUID",
					tag,
					ValueRepresentation.UI,
					"1",
					"Contains the UID of the SOP Instance for which this operation occurred.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1001);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Requested SOP Instance UID",
					"RequestedSOPInstanceUID",
					tag,
					ValueRepresentation.UI,
					"1",
					"Contains the UID of the SOP Instance for which this operation occurred.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1002);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Event Type ID",
					"EventTypeID",
					tag,
					ValueRepresentation.US,
					"1",
					"Values for this field are application-specific.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1005);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Attribute Identifier List",
					"AttributeIdentifierList",
					tag,
					ValueRepresentation.AT,
					"1-n",
					"This field contains an Attribute Tag for each of the n Attributes applicable.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1008);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Action Type ID",
					"ActionTypeID",
					tag,
					ValueRepresentation.US,
					"1",
					"Values for this field are application-specific.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1020);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Number of Remaining Sub-operations",
					"NumberOfRemainingSuboperations",
					tag,
					ValueRepresentation.US,
					"1",
					"The number of remaining C-STORE suboperations to be invoked for the operation.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1021);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Number of Completed Sub-operations",
					"NumberOfCompletedSuboperations",
					tag,
					ValueRepresentation.US,
					"1",
					"The number of C-STORE sub-operations associated with this operation that have completed successfully.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1022);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Number of Failed Sub-operations",
					"NumberOfFailedSuboperations",
					tag,
					ValueRepresentation.US,
					"1",
					"The number of C-STORE sub-operations associated with this operation that have failed.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1023);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Number of Warning Sub-operations",
					"NumberOfWarningSuboperations",
					tag,
					ValueRepresentation.US,
					"1",
					"The number of C-STORE sub-operations associated with this operation that generated warning responses.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1030);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Move Originator Application Entity Title",
					"MoveOriginatorApplicationEntityTitle",
					tag,
					ValueRepresentation.AE,
					"1",
					"Contains the DICOM AE Title of the DICOM AE that invoked the C-MOVE operation from which this C-STORE sub-operation is being performed.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x1031);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Move Originator Message ID",
					"MoveOriginatorMessageID",
					tag,
					ValueRepresentation.US,
					"1",
					"Contains the Message ID (0000,0110) of the C-MOVE-RQ Message from which this C-STORE sub-operation is being performed.",
					false);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0001);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Length to End",
					"CommandLengthToEnd",
					tag,
					ValueRepresentation.UL,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0010);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Recognition Code",
					"CommandRecognitionCode",
					tag,
					ValueRepresentation.CS,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0200);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Initiator",
					"Initiator",
					tag,
					ValueRepresentation.AE,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0300);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Receiver",
					"Receiver",
					tag,
					ValueRepresentation.AE,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0400);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Find Location",
					"FindLocation",
					tag,
					ValueRepresentation.AE,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0850);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Number of Matches",
					"NumberOfMatches",
					tag,
					ValueRepresentation.US,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x0860);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Response Sequence Number",
					"ResponseSequenceNumber",
					tag,
					ValueRepresentation.US,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x4000);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"DIALOG Receiver",
					"DialogReceiver",
					tag,
					ValueRepresentation.AT,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x4010);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Terminal Type",
					"TerminalType",
					tag,
					ValueRepresentation.AT,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5010);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Message Set ID",
					"MessageSetID",
					tag,
					ValueRepresentation.SH,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5020);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"End Message ID",
					"EndMessageID",
					tag,
					ValueRepresentation.SH,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5110);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Display Format",
					"DisplayFormat",
					tag,
					ValueRepresentation.AT,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5120);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Page Position ID",
					"PagePositionID",
					tag,
					ValueRepresentation.AT,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5130);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Text Format ID",
					"TextFormatID",
					tag,
					ValueRepresentation.CS,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5140);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Normal/Reverse",
					"NormalReverse",
					tag,
					ValueRepresentation.CS,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5150);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Add Gray Scale",
					"AddGrayScale",
					tag,
					ValueRepresentation.CS,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5160);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Borders",
					"Borders",
					tag,
					ValueRepresentation.CS,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5170);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Copies",
					"Copies",
					tag,
					ValueRepresentation.IS,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5180);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Magnification Type",
					"CommandMagnificationType",
					tag,
					ValueRepresentation.CS,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x5190);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Erase",
					"Erase",
					tag,
					ValueRepresentation.CS,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x51A0);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Print",
					"Print",
					tag,
					ValueRepresentation.CS,
					"1",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}

		{
			Tag tag = new Tag(0x0000, 0x51B0);
			CommandDictionaryEntry e = new CommandDictionaryEntry(
					"Overlays",
					"Overlays",
					tag,
					ValueRepresentation.US,
					"1-n",
					"",
					true);
			CommandDictionary.defaultDictionary.put(tag, e);
		}


	}
	
	public CommandDictionaryEntry getCommandGroupLength()
	{
		Tag tag = new Tag(0x0000, 0x0000);
		return this.get(tag);
	}

	public CommandDictionaryEntry getAffectedSOPClassUID()
	{
		Tag tag = new Tag(0x0000, 0x0002);
		return this.get(tag);
	}

	public CommandDictionaryEntry getRequestedSOPClassUID()
	{
		Tag tag = new Tag(0x0000, 0x0003);
		return this.get(tag);
	}

	public CommandDictionaryEntry getCommandField()
	{
		Tag tag = new Tag(0x0000, 0x0100);
		return this.get(tag);
	}

	public CommandDictionaryEntry getMessageID()
	{
		Tag tag = new Tag(0x0000, 0x0110);
		return this.get(tag);
	}

	public CommandDictionaryEntry getMessageIDBeingRespondedTo()
	{
		Tag tag = new Tag(0x0000, 0x0120);
		return this.get(tag);
	}

	public CommandDictionaryEntry getMoveDestination()
	{
		Tag tag = new Tag(0x0000, 0x0600);
		return this.get(tag);
	}

	public CommandDictionaryEntry getPriority()
	{
		Tag tag = new Tag(0x0000, 0x0700);
		return this.get(tag);
	}

	public CommandDictionaryEntry getCommandDataSetType()
	{
		Tag tag = new Tag(0x0000, 0x0800);
		return this.get(tag);
	}

	public CommandDictionaryEntry getStatus()
	{
		Tag tag = new Tag(0x0000, 0x0900);
		return this.get(tag);
	}

	public CommandDictionaryEntry getOffendingElement()
	{
		Tag tag = new Tag(0x0000, 0x0901);
		return this.get(tag);
	}

	public CommandDictionaryEntry getErrorComment()
	{
		Tag tag = new Tag(0x0000, 0x0902);
		return this.get(tag);
	}

	public CommandDictionaryEntry getErrorID()
	{
		Tag tag = new Tag(0x0000, 0x0903);
		return this.get(tag);
	}

	public CommandDictionaryEntry getAffectedSOPInstanceUID()
	{
		Tag tag = new Tag(0x0000, 0x1000);
		return this.get(tag);
	}

	public CommandDictionaryEntry getRequestedSOPInstanceUID()
	{
		Tag tag = new Tag(0x0000, 0x1001);
		return this.get(tag);
	}

	public CommandDictionaryEntry getEventTypeID()
	{
		Tag tag = new Tag(0x0000, 0x1002);
		return this.get(tag);
	}

	public CommandDictionaryEntry getAttributeIdentifierList()
	{
		Tag tag = new Tag(0x0000, 0x1005);
		return this.get(tag);
	}

	public CommandDictionaryEntry getActionTypeID()
	{
		Tag tag = new Tag(0x0000, 0x1008);
		return this.get(tag);
	}

	public CommandDictionaryEntry getNumberOfRemainingSuboperations()
	{
		Tag tag = new Tag(0x0000, 0x1020);
		return this.get(tag);
	}

	public CommandDictionaryEntry getNumberOfCompletedSuboperations()
	{
		Tag tag = new Tag(0x0000, 0x1021);
		return this.get(tag);
	}

	public CommandDictionaryEntry getNumberOfFailedSuboperations()
	{
		Tag tag = new Tag(0x0000, 0x1022);
		return this.get(tag);
	}

	public CommandDictionaryEntry getNumberOfWarningSuboperations()
	{
		Tag tag = new Tag(0x0000, 0x1023);
		return this.get(tag);
	}

	public CommandDictionaryEntry getMoveOriginatorApplicationEntityTitle()
	{
		Tag tag = new Tag(0x0000, 0x1030);
		return this.get(tag);
	}

	public CommandDictionaryEntry getMoveOriginatorMessageID()
	{
		Tag tag = new Tag(0x0000, 0x1031);
		return this.get(tag);
	}

	public CommandDictionaryEntry getCommandLengthToEnd()
	{
		Tag tag = new Tag(0x0000, 0x0001);
		return this.get(tag);
	}

	public CommandDictionaryEntry getCommandRecognitionCode()
	{
		Tag tag = new Tag(0x0000, 0x0010);
		return this.get(tag);
	}

	public CommandDictionaryEntry getInitiator()
	{
		Tag tag = new Tag(0x0000, 0x0200);
		return this.get(tag);
	}

	public CommandDictionaryEntry getReceiver()
	{
		Tag tag = new Tag(0x0000, 0x0300);
		return this.get(tag);
	}

	public CommandDictionaryEntry getFindLocation()
	{
		Tag tag = new Tag(0x0000, 0x0400);
		return this.get(tag);
	}

	public CommandDictionaryEntry getNumberOfMatches()
	{
		Tag tag = new Tag(0x0000, 0x0850);
		return this.get(tag);
	}

	public CommandDictionaryEntry getResponseSequenceNumber()
	{
		Tag tag = new Tag(0x0000, 0x0860);
		return this.get(tag);
	}

	public CommandDictionaryEntry getDialogReceiver()
	{
		Tag tag = new Tag(0x0000, 0x4000);
		return this.get(tag);
	}

	public CommandDictionaryEntry getTerminalType()
	{
		Tag tag = new Tag(0x0000, 0x4010);
		return this.get(tag);
	}

	public CommandDictionaryEntry getMessageSetID()
	{
		Tag tag = new Tag(0x0000, 0x5010);
		return this.get(tag);
	}

	public CommandDictionaryEntry getEndMessageID()
	{
		Tag tag = new Tag(0x0000, 0x5020);
		return this.get(tag);
	}

	public CommandDictionaryEntry getDisplayFormat()
	{
		Tag tag = new Tag(0x0000, 0x5110);
		return this.get(tag);
	}

	public CommandDictionaryEntry getPagePositionID()
	{
		Tag tag = new Tag(0x0000, 0x5120);
		return this.get(tag);
	}

	public CommandDictionaryEntry getTextFormatID()
	{
		Tag tag = new Tag(0x0000, 0x5130);
		return this.get(tag);
	}

	public CommandDictionaryEntry getNormalReverse()
	{
		Tag tag = new Tag(0x0000, 0x5140);
		return this.get(tag);
	}

	public CommandDictionaryEntry getAddGrayScale()
	{
		Tag tag = new Tag(0x0000, 0x5150);
		return this.get(tag);
	}

	public CommandDictionaryEntry getBorders()
	{
		Tag tag = new Tag(0x0000, 0x5160);
		return this.get(tag);
	}

	public CommandDictionaryEntry getCopies()
	{
		Tag tag = new Tag(0x0000, 0x5170);
		return this.get(tag);
	}

	public CommandDictionaryEntry getCommandMagnificationType()
	{
		Tag tag = new Tag(0x0000, 0x5180);
		return this.get(tag);
	}

	public CommandDictionaryEntry getErase()
	{
		Tag tag = new Tag(0x0000, 0x5190);
		return this.get(tag);
	}

	public CommandDictionaryEntry getPrint()
	{
		Tag tag = new Tag(0x0000, 0x51A0);
		return this.get(tag);
	}

	public CommandDictionaryEntry getOverlays()
	{
		Tag tag = new Tag(0x0000, 0x51B0);
		return this.get(tag);
	}
}
