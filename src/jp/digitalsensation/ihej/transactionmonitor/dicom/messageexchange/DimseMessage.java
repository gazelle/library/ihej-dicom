package jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange;

import java.io.File;

public class DimseMessage {
	private final String transferSyntax;
	public String getTransferSyntax()
	{
		return this.transferSyntax;
	}
	
	private final byte[] commandSet;
	public byte[] getCommandSet()
	{
		return this.commandSet.clone();
	}
	
	private final byte[] dataSet;

	public byte[] getDataSet()
	{
		return this.dataSet;
	}
	
	public DimseMessage(
			String transferSyntax,
			byte[] commandSet,
			byte[] dataSet) {
		this.transferSyntax = transferSyntax;
		this.commandSet = commandSet.clone();
		this.dataSet = dataSet;
	}
}
