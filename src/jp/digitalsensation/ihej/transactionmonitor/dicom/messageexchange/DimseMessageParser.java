package jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange;

import jp.digitalsensation.ihej.transactionmonitor.dicom.DicomParserException;
import jp.digitalsensation.ihej.transactionmonitor.dicom.datastructure.Tag;
import jp.digitalsensation.ihej.transactionmonitor.dicom.upperlayerprotocol.*;
import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Implementation;
import org.dcm4che3.data.VR;
import org.dcm4che3.io.DicomOutputStream;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import static jp.digitalsensation.ihej.transactionmonitor.utils.ObjectDump.dump;

public class DimseMessageParser {

    private final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(
            "yyyyMMdd-HHmmss.SSS");
    private final Association association;
    private final DicomULParser pduParser;
    private ByteArrayOutputStream commandSet;
    private boolean commandSetCompleted;;
    private boolean dataSetCompleted;

    private ByteArrayOutputStream dataSet;

    private DicomOutputStream datasetStream;
    private Attributes fmi;

    public DimseMessageParser() throws IOException {
        this(new Association());
    }



    //TODO : For the moment this method does not do anything
    //  private void moveDataSetFileToHierarchy() {
    //     if (this.getRootForDataSet() != null) {
    //TODO : This needs to be completed

    //     }
    // }

    public DimseMessageParser(Association association)
            throws IOException {
        this.association = association;
        this.pduParser = new DicomULParser();
        this.commandSet = new ByteArrayOutputStream(8196);
        this.dataSet = new ByteArrayOutputStream();
        this.clearBuffer();
    }

    public static void main(String[] args) {
        for (String arg : args) {
            try {
                byte[] buffer = new byte[8192];
                java.io.FileInputStream fis = new java.io.FileInputStream(arg);
                DimseMessageParser p = new DimseMessageParser();

                int offset = 0;
                while (true) {
                    int read = fis.read(buffer, offset, 8192);
                    if (read == -1) {
                        break;
                    }

                    byte[] data = new byte[read];
                    System.arraycopy(buffer, 0, data, 0, read);

                    for (DimseMessage message : p.push(data)) {
                        dump(message);
                        System.out.println();
                    }
                }
                ;

                dump(p.getAssociation());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public Association getAssociation() {
        return this.association;
    }




    private void clearBuffer() throws IOException {
        this.commandSet.reset();
        this.commandSetCompleted = false;
        this.dataSetCompleted = false;
        this.dataSet.reset();
        this.datasetStream = null;
    }

    public Iterable<DimseMessage> push(byte[] data)
            throws DicomParserException, IOException {
        Iterable<ProtocolDataUnit> pdus = this.pduParser.push(data);
        return this.push(pdus);
    }

    public Iterable<DimseMessage> push(Iterable<ProtocolDataUnit> pdus)
            throws DicomParserException, IOException {
        ArrayList<DimseMessage> result = new ArrayList<DimseMessage>();

        for (ProtocolDataUnit pdu : pdus) {
            switch (pdu.getPDUType()) {
                case ProtocolDataUnitTypes.A_ASSOCIATE_RQ:
                    this.handleAssociateRequest((A_ASSOCIATE_RQ_PDU) pdu);
                    break;

                case ProtocolDataUnitTypes.A_ASSOCIATE_AC:
                    this.handleAssociateAcceptance((A_ASSOCIATE_AC_PDU) pdu);
                    break;

                case ProtocolDataUnitTypes.P_DATA_TF:
                    result.addAll(this.handleDataTransfer((P_DATA_TF_PDU) pdu));
                    break;

                default:
                    break;
            }
        }

        return result;
    }

    private void handleAssociateRequest(A_ASSOCIATE_RQ_PDU pdu) {
        this.association.setCallingApplicationEntityTitle(pdu
                .getCallingEntityTitle());
        this.association.setCalledApplicationEntityTitle(pdu
                .getCalledEntityTitle());
        for (Item item : pdu.getVariableItems()) {
            if (item instanceof UserInformationItem) {
                for (Item ud : ((UserInformationItem) item).getUserData()) {
                    if (ud instanceof ImplementationClassUIDItem) {
                        this.association
                                .setCallingApplicationEntityImplementationClassUid(((ImplementationClassUIDItem) ud)
                                        .getImplementationClassUID());
                    } else if (ud instanceof ImplementationVersionNameItem) {
                        this.association
                                .setCallingApplicationEntityImplementationVersion(((ImplementationVersionNameItem) ud)
                                        .getImplementationVersionName());
                    }
                }
            }
        }
    }

    private void handleAssociateAcceptance(A_ASSOCIATE_AC_PDU pdu) {
        if (this.association.getCalledApplicationEntityTitle() == null) {
            this.association.setCalledApplicationEntityTitle(pdu
                    .getCalledEntityTitle());
        }
        if (this.association.getCallingApplicationEntityTitle() == null) {
            this.association.setCallingApplicationEntityTitle(pdu
                    .getCallingEntityTitle());
        }
        for (Item item : pdu.getVariableItems()) {
            if (item instanceof UserInformationItem) {
                for (Item ud : ((UserInformationItem) item).getUserData()) {
                    if (ud instanceof ImplementationClassUIDItem) {
                        this.association
                                .setCalledApplicationEntityImplementationClassUid(((ImplementationClassUIDItem) ud)
                                        .getImplementationClassUID());
                    } else if (ud instanceof ImplementationVersionNameItem) {
                        this.association
                                .setCalledApplicationEntityImplementationVersion(((ImplementationVersionNameItem) ud)
                                        .getImplementationVersionName());
                    }
                }
            } else if (item instanceof PresentationContextResultItem) {
                PresentationContextResultItem pcr = (PresentationContextResultItem) item;
                if (pcr.getResult() == PresentationContextResultItem.Result.Acceptance) {
                    for (Item si : pcr.getSubItems()) {
                        if (si instanceof TransferSyntaxItem) {
                            short id = pcr.getPresentationContextID();
                            String ts = ((TransferSyntaxItem) si)
                                    .getTransferSyntax();
                            this.association.getPresentationContexts().put(id,
                                    new PresentationContext(id, ts));
                        }
                    }
                }
            }
        }
    }

    private ArrayList<DimseMessage> handleDataTransfer(P_DATA_TF_PDU pdu)
            throws DicomParserException, IOException {
        ArrayList<DimseMessage> result = new ArrayList<DimseMessage>();

        for (PresentationDataValueItem item : pdu.getPresentationDataValueItems()) {
            PresentationDataValue pdv = item.getPresentationDataValue();
            if (pdv.isCommandFragment()) {
                this.commandSet.write(pdv.getData());
                if (pdv.isLastFragment()) {
                    this.commandSetCompleted = true;

                    fmi = new Attributes();

                    CommandSetReader r = new CommandSetReader(
                            new ByteArrayInputStream(commandSet.toByteArray()));
                    String transferSyntax = getTransferSyntax(item);
                    // AffectedSOPClassUID
                    String cuid = getTagValue(r, CommandDictionary.getDefault()
                            .getAffectedSOPClassUID().getTag());
                    // AffectedSOPInstanceUID
                    String iuid = getTagValue(r, CommandDictionary.getDefault()
                            .getAffectedSOPInstanceUID().getTag());

                    initFileMetaInformation(cuid, iuid, transferSyntax);
                }
            } else {
                if(this.datasetStream == null){
                    this.datasetStream = new DicomOutputStream(dataSet,"1.2.840.10008.1.2.1");
                    this.datasetStream.writeFileMetaInformation(fmi);
                }
                this.datasetStream.write(pdv.getData());
                if (pdv.isLastFragment()) {
                    this.dataSetCompleted = true;
                }
            }

            if ((this.commandSetCompleted && this.dataSetCompleted)
                    || (pdv.isCommandFragment() && pdv.isLastFragment() && !this
                    .hasDataSet(this.commandSet))) {
                String transferSyntax = getTransferSyntax(item);

                //TODO: what is exactly expected here. This method does not do anything. We need to understand what
                //was the expectation
                //     moveDataSetFileToHierarchy();

                DimseMessage dm;
                if (this.dataSetCompleted) {
                    dm = new DimseMessage(transferSyntax,
                            this.commandSet.toByteArray(), dataSet.toByteArray());
                } else {
                    dm = new DimseMessage(transferSyntax,
                            this.commandSet.toByteArray(), null);
                }

                result.add(dm);
                this.clearBuffer();
            }
        }

        return result;
    }

    private void initFileMetaInformation(String cuid, String iuid, String tsuid) {
        this.fmi.setBytes(131073, VR.OB, new byte[]{0, 1});
        this.fmi.setString(131074, VR.UI, cuid);
        this.fmi.setString(131075, VR.UI, iuid);
        this.fmi.setString(131088, VR.UI, tsuid);
        this.fmi.setString(131090, VR.UI, Implementation.getClassUID());
        this.fmi.setString(131091, VR.SH, Implementation.getVersionName());
    }

    private String getTransferSyntax(PresentationDataValueItem item) {
        short pcid = item.getPresentationContextID();
        PresentationContext pc = this.association.getPresentationContexts()
                .get(pcid);
        String transferSyntax;
        if (pc != null) {
            transferSyntax = pc.getTransferSyntax();
        } else {
            // FIXME: no transfer syntax found. occurs when association
            // is not shared between upstream(requesting) and
            // downstream(accepting)
            // throw new DicomParserException(
            // "Error while reconstructing dicom message, presentation context not recognized.");

            // transferSyntax = "1.2.840.10008.1.2";

            transferSyntax = null;
        }
        return transferSyntax;
    }

    private String getTagValue(CommandSetReader r, Tag tag)
            throws DicomParserException, IOException, IllegalAccessError {
        String result = null;
        boolean moveTo = r.moveTo(tag.getGroupNumber(), tag.getElementNumber());
        if (moveTo) {
            byte[] value = r.getAttribute().getValue();
            result = new String(value);
        }
        return result;
    }

    private boolean hasDataSet(ByteArrayOutputStream commandSet)
            throws DicomParserException, IOException {
        Tag tag = CommandDictionary.getDefault().getCommandDataSetType()
                .getTag();

        CommandSetReader r = new CommandSetReader(new ByteArrayInputStream(
                commandSet.toByteArray()));
        if (!r.moveTo(tag.getGroupNumber(), tag.getElementNumber())) {
            throw new DicomParserException(
                    "Error reading command set. \"Data Set Type\" attribute not found.");
        }

        try {
            return (!(r.getUSValue() == 0x0101));
        } catch (Exception ex) {
            throw new DicomParserException(
                    "Error reading command set. couldn't read \"Data Set Type\" attribute : "
                            + ex.getMessage(), ex);
        }
    }
}
