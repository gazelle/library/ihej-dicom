package jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange;

public class PresentationContext
{
	private final short id;
	public short getID()
	{
		return this.id;
	}
	
	private final String transferSyntax;
	public String getTransferSyntax()
	{
		return this.transferSyntax;
	}
	
	public PresentationContext(short id, String transferSyntax)
	{
		this.id = id;
		this.transferSyntax = transferSyntax;
	}
}
