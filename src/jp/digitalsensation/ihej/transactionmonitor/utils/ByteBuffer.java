package jp.digitalsensation.ihej.transactionmonitor.utils;

public class ByteBuffer
{
	private byte[] array;
	public byte[] getArray()
	{
		return this.array.clone();
	}
	
	private int offset;
	public int getOffset()
	{
		return this.offset;
	}
	
	private int length;
	public int getLength()
	{
		return this.length;
	}
	
	public int getCapacity()
	{
		return this.array.length;
	}
	
	public void consume(int length)
		throws IllegalArgumentException
	{
		if (!(0 <= length && length <= this.length))
		{
			throw new IllegalArgumentException();
		}
		
		this.offset += length;
		this.length -= length;
	}
	
	public void append(byte[] data)
	{
		this.append(data, 0, data.length);
	}
	
	public void append(byte[] data, int offset, int length)
	{
		if (data == null)
		{
			throw new IllegalArgumentException();
		}
		if (!(0 <= offset))
		{
			throw new IllegalArgumentException();
		}
		if (!(0 <= length))
		{
			throw new IllegalArgumentException();
		}
		if (!(offset + length <= data.length))
		{
			throw new IllegalArgumentException();
		}

		if (this.array.length < length + this.length)
		{
			byte[] newArray = new byte[this.array.length * 2];
			System.arraycopy(this.array, this.offset, newArray, 0, this.length);
			this.array = newArray;
			this.offset = 0;
		}
		else if ((this.array.length - (this.offset + this.length)) < length)
		{
			System.arraycopy(this.array, this.offset, this.array, 0, this.length);
			this.offset = 0;
		}
		
		// copy data
		System.arraycopy(data, offset, this.array, this.offset + this.length, length);
		this.length += length;
	}
	
	public ByteBuffer()
	{
		this(10);
	}
	
	public ByteBuffer(int capacity)
	{
		this(new byte[capacity], 0, 0);
	}
	
	public ByteBuffer(byte[] array)
	{
		this(array, 0, array.length);
	}
	
	public ByteBuffer(byte[] array, int offset, int length)
		throws IllegalArgumentException
	{
		if (array == null)
		{
			throw new IllegalArgumentException();
		}
		if (!(0 <= offset))
		{
			throw new IllegalArgumentException();
		}
		if (!(0 <= length))
		{
			throw new IllegalArgumentException();
		}
		if (!(offset + length <= array.length))
		{
			throw new IllegalArgumentException();
		}
		
		this.array = array.clone();
		this.offset = offset;
		this.length = length;
	}
	
	public void clear()
	{
		this.offset = 0;
		this.length = 0;
	}
}
