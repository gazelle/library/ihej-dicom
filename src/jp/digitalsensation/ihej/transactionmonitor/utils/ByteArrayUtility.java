package jp.digitalsensation.ihej.transactionmonitor.utils;

public final class ByteArrayUtility
{
	public static short getUnsignedByte(byte[] array, int offset)
	{
		return (short)(array[offset] & 0xFF);
	}
	
	public static int getUnsignedShortBigEndian(byte[] array, int offset)
	{
		return
			((array[offset + 0] & 0xFF) << 8) +
			(array[offset + 1] & 0xFF);
	}
	
	public static int getUnsignedShortLittleEndian(byte[] array, int offset)
	{
		return
			(array[offset + 0] & 0xFF) +
			((array[offset + 1] & 0xFF) << 8);
	}
	
	public static long getUnsignedIntegerBigEndian(byte[] array, int offset)
	{
		return
			(((long)array[offset + 0] & 0xFF) << 24) +
			(((long)array[offset + 1] & 0xFF) << 16) +
			(((long)array[offset + 2] & 0xFF) << 8) +
			((long)array[offset + 3] & 0xFF);
	}
	
	public static long getUnsignedIntegerLittleEndian(byte[] array, int offset)
	{
		return
		((long)array[offset + 0] & 0xFF) +
		(((long)array[offset + 1] & 0xFF) << 8) +
		(((long)array[offset + 2] & 0xFF) << 16) +
		(((long)array[offset + 3] & 0xFF) << 24);
	}
	
	public static String getString(byte[] array, int offset, int length)
	{
		return new String(array, offset, length);
	}
	
	public static byte[] getBytes(byte[] array, int offset, int length)
	{
		byte[] newBuffer = new byte[length];
		System.arraycopy(array, offset, newBuffer, 0, length);
		return newBuffer;
	}
	
	public static byte[] concatinateBytes(byte[] left, byte[] right)
	{
		byte[] newArray = new byte[left.length + right.length];
		System.arraycopy(left, 0, newArray, 0, left.length);
		System.arraycopy(right, 0, newArray, left.length, right.length);
		
		return newArray;
	}
	
	public static byte[] dropBytes(byte[] array, int offset, int length)
	{
		// TODO: check arguments
		byte[] newArray = new byte[array.length - length];
		
		if (offset != 0)
		{
			System.arraycopy(array, 0, newArray, 0, offset);
		}
		System.arraycopy(array, offset + length, newArray, offset, array.length - (offset + length));
		
		return newArray;
	}
}
