package jp.digitalsensation.ihej.transactionmonitor.utils;

import java.io.PrintStream;
import java.lang.reflect.*;

public class ObjectDump
{
	public static void dump(Object obj)
	{
		dump(obj.getClass().getSimpleName(), obj);
	}
	
	public static void dump(String label, Object obj)
	{
		dump(label, obj, 0, System.out);
	}
	
	private static void dump(String label, Object obj, int indent, PrintStream ps)
	{
		doIndent(indent, ps);
		
		ps.print(label);
		for (int i = label.length() + indent * 2; i < 32; i++)
		{
			ps.print(" ");
		}
		ps.print(": ");
		
		if (obj == null)
		{
			ps.println("null");
			return;
		}
		else
		{
			ps.println(toSimpleString(obj));
		}

		Class<? extends Object> cls = obj.getClass();
		if (cls.isPrimitive() /* never happens */ ||
				obj instanceof Boolean ||
				obj instanceof Byte ||
				obj instanceof Character ||
				obj instanceof Short ||
				obj instanceof Integer ||
				obj instanceof Long ||
				obj instanceof Float ||
				obj instanceof Double ||
				obj instanceof String)
		{
			return;
		}

		doIndent(indent, ps);
		ps.println('{');
		
		if (obj instanceof byte[])
		{
			hexDump((byte[])obj, System.out, indent + 1, 48);
		}
		else if (cls.isArray())
		{
			int length = java.lang.reflect.Array.getLength(obj);
			
			for (int i = 0; i < length; i++)
			{
				Object value = java.lang.reflect.Array.get(obj, i);
				String itemLabel = label + "[" + Integer.toString(i) + "]";
				dump(itemLabel, value, indent + 1, ps);
			}
		}
		else if (obj instanceof java.lang.Iterable<?>)
		{
			int index = 0;
			for (Object item : (java.lang.Iterable<?>)obj)
			{
				String itemLabel = label + "[" + Integer.toString(index) + "]";
				dump(itemLabel, item, indent + 1, ps);
				index++;
			}
		}
		else if (obj instanceof java.util.Map<?, ?>)
		{
			int index = 0;
			for (java.util.Map.Entry<?, ?> item : ((java.util.Map<?, ?>)obj).entrySet())
			{
				String itemLabel = label + "[" + toSimpleString(item.getKey()) + "]";
				dump(itemLabel, item.getValue(), indent + 1, ps);
				index++;
			}
		}
		else
		{
			for (Method m : cls.getMethods())
			{
				String propertyName = m.getName();
				if (!Modifier.isStatic(m.getModifiers()) &&
					m.getParameterTypes().length == 0 &&
					m.getReturnType() != java.lang.Void.class &&
					(
						m.getName().startsWith("get") ||
						m.getName().startsWith("is") ||
						m.getName().startsWith("has")
					) &&
					m.getDeclaringClass() != java.lang.Object.class)
				{
					if (propertyName.startsWith("get"))
					{
						propertyName = propertyName.substring(3);
					}
					m.setAccessible(true);
					Object propertyValue;
					try
					{
						propertyValue = m.invoke(obj); 
					}
					catch (Exception ex)
					{
						propertyValue = "<" + ex.getMessage() + ">";
					}
					
					dump(propertyName, propertyValue, indent + 1, ps);
				}
			}
		}
		
		doIndent(indent, ps);
		ps.println('}');
	}
	
	private static void doIndent(int indent, PrintStream ps)
	{
		for (int i = 0; i < indent; i++)
		{
			ps.print("  ");
		}
	}
	
	public static void hexDump(byte[] data, PrintStream ps, int indent, int limit)
	{
		for (int i = 0; i < data.length && i < limit; i += 16)
		{
			doIndent(indent, ps);
			ps.printf("%08X : ", i);
			
			for (int j = 0; j < 16 && i + j < data.length && i + j < limit; j++)
			{
				if (j == 8)
				{
					ps.print(' ');
				}
				
				ps.printf("%02X ", data[i + j]);
			}
			
			ps.println();
		}
		
		if (limit < data.length)
		{
			doIndent(indent, ps);
			ps.println("... " + Integer.toString(data.length - limit) + " byte skipped. (" + Integer.toString(data.length) + " byte total)");
		}
	}
	
	public static String toSimpleString(Object obj)
	{
		if (obj == null)
		{
			return "null";
		}
		
		Class<? extends Object> cls = obj.getClass();
		try
		{
			Method m = cls.getMethod("toString");
			String declaringClass = m.getDeclaringClass().getCanonicalName();

			if (declaringClass.equals("java.lang.Object"))
			{
				return cls.getSimpleName();
			}
			else if (declaringClass.equals("java.util.AbstractCollection") ||
						declaringClass.equals("java.util.Collections.UnmodifiableCollection"))
			{
				int size = ((java.util.Collection<?>)obj).size();
				
				return cls.getSimpleName() + "[" + Integer.toString(size) + "]";
			}
			else if (declaringClass.equals("java.util.AbstractMap"))
			{
				int size = ((java.util.Map<?, ?>)obj).size();

				return cls.getSimpleName() + "[" + Integer.toString(size) + "]";
			}
			else
			{
				String[] lines = obj.toString().split("\n");
				if (lines.length > 1)
				{
					return lines[0] + "...";
				}
				else
				{
					return lines[0];
				}
			}
		}
		catch (Exception ex)
		{
			throw new java.lang.RuntimeException(ex);
		}
	}
}
